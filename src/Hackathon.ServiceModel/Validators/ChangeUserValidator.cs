﻿//
//  ChangeUserValidator.cs
//
//  Author:
//       Mathias Tausig <mathias.tausig@fh-campuswien.ac.at>
//
//  Copyright (c) 2016 University of Applied Sciences Campus Vienna
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Text.RegularExpressions;
using ServiceStack.FluentValidation;
using ServiceStack;

namespace Hackathon.ServiceModel.Validators
{
	public class ChangeUserValidator: AbstractValidator<ChangeUser>
	{
		public ChangeUserValidator ()
		{
			//Validation rules for all requests
			RuleFor (u => u.Avatar).Must (a => a.IsNullOrEmpty () || Regex.IsMatch (a, @"^data:image/[a-z]+;base64, [a-zA-Z0-9\+/=\s]*$"));
		}
	}
}

